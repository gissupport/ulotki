    var myToken;
  
  (function() {
  
  if(typeof DIVI_BASE_URL === 'undefined') {
      DIVI_BASE_URL = 'http://divi.io/';
  }
  if (typeof String.prototype.trimLeft !== "function") {
      String.prototype.trimLeft = function() {
          return this.replace(/^\s+/, "");
      };
  }
  if (typeof String.prototype.trimRight !== "function") {
      String.prototype.trimRight = function() {
          return this.replace(/\s+$/, "");
      };
  }
  
  var URLparameters = function () {
    // http://stackoverflow.com/questions/979975/how-to-get-the-value-from-the-url-parameter?page=1&tab=votes#tab-top
    // This function is anonymous, is executed immediately and
    // the return value is assigned to QueryString!
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
      var pair = vars[i].split("=");
          // If first entry with this name
      if (typeof query_string[pair[0]] === "undefined") {
        query_string[pair[0]] = decodeURIComponent(pair[1]);
          // If second entry with this name
      } else if (typeof query_string[pair[0]] === "string") {
        var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
        query_string[pair[0]] = arr;
          // If third or later entry with this name
      } else {
        query_string[pair[0]].push(decodeURIComponent(pair[1]));
      }
    }
      return query_string;
  };
  
  var getCookies = function() {
      var c = document.cookie, v = 0, cookies = {};
      if (document.cookie.match(/^\s*\$Version=(?:"1"|1);\s*(.*)/)) {
          c = RegExp.$1;
          v = 1;
      }
      if (v === 0) {
          c.split(/[,;]/).map(function(cookie) {
              var parts = cookie.split(/=/, 2),
                  name = decodeURIComponent(parts[0].trimLeft()),
                  value = parts.length > 1 ? decodeURIComponent(parts[1].trimRight()) : null;
              cookies[name] = value;
          });
      } else {
          c.match(/(?:^|\s+)([!#$%&'*+\-.0-9A-Z^`a-z|~]+)=([!#$%&'*+\-.0-9A-Z^`a-z|~]*|"(?:[\x20-\x7E\x80\xFF]|\\[\x00-\x7F]*)")(?=\s*[,;]|$)/g).map(function($0, $1) {
              var name = $0,
                  value = $1.charAt(0) === '"'
                            ? $1.substr(1, -1).replace(/\\(.)/g, "$1")
                            : $1;
              cookies[name] = value;
          });
      }
      return cookies;
  }
  var getCookie = function(name) {
      return getCookies()[name];
  }
  
  var QueryString = URLparameters();
  myToken = QueryString.token;
  
  if(myToken) {
      localStorage.setItem('accesstoken',myToken);
  }
  
  else {
      myToken = localStorage.getItem('accesstoken');
  }
  
  if(!myToken || myToken === 'undefined') {
    myToken = getCookie('accesstoken');
  }
  
  if(!myToken || myToken === 'undefined') {
    //$("#logged").hide();
    //$("#notLogged").show();
    var urlToReplace = DIVI_BASE_URL+"login?next="+location.origin+location.pathname;
    window.location.replace(urlToReplace);
  } else {
  
  }
  
  })();
