from peewee import SqliteDatabase, Model, IntegerField, CharField, Field

db = SqliteDatabase('baza_wwa.sqlite')

class GeometryField(Field):
    field_type = "geometry"

class Adres(Model):
    ogc_fid = IntegerField(primary_key=True)
    ulica = CharField()
    kodpocztow = CharField()
    x_92 = GeometryField()
    y_92 = GeometryField()
    numer = CharField()

    class Meta:
        database = db
        table_name = 'baza_wwa'