from flask import Flask, render_template, jsonify, request
from models import db, Adres
import os

app = Flask(__name__)
app._db = db

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/baza')
def baza():

    input = request.args.get('w')

    o = Adres.select().where(Adres.ulica.concat(' ').concat(Adres.numer).contains(input)).limit(20)
    features = []
    for obj in o:
        features.append({
                    "ulica":obj.ulica,
                    "numer":obj.numer,
                    "kod": obj.kodpocztow,
                    "x": obj.x_92,
                    "y": obj.y_92
        })
    json_dict = {
        "features": features
    }
    return jsonify(json_dict)

if __name__ == '__main__':
   app.run(debug=True)