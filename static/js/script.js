var url = "https://divi.io/api/json/MzUy.OSq_UP391r6Tvbw3XKywmkzCX-8"; 

$.getJSON(url, function(data) {
  var html = '';
  var len = data.data.length;
  for (var i = 0; i < len; i++) {
    html += '<option value="' + data.data[i].nazwa + '">' +    data.data[i].nazwa + '</option>';
  }
  $("#firma").append(html);
});


var app = new Vue({
  el: '#app',
  data: {
    mail:'',
    date:'',
    company:'',
    fliers:'',
    rowData:[],
    query: '',
    baza: []
  },
  components: {
    vuejsDatepicker,
    VueBootstrapTypeahead
  },
  watch: {
    query(newQuery) {
      axios.get(`/baza?w=${newQuery}`)
        .then((res) => {
          this.baza = res.data.features
        })
    }
  },
  methods:{
    addItem(){
      var my_object = {
        mail:this.mail,
        date:this.date.toLocaleDateString('pl', {day: 'numeric', month: 'numeric',year: 'numeric'}),
        company:this.company,
        ulica:this.baza[0].ulica,
        numer:this.baza[0].numer,
        kod:this.baza[0].kod,
        x:this.baza[0].x,
        y:this.baza[0].y,
        fliers:this.fliers
      };
      this.rowData.push(my_object)
    },
  },
})



function doCsv() {
  var table = document.getElementById("mytable").innerHTML;
  var data = table.replace(/<thead>/g, '')
      .replace(/<\/thead>/g, '')
      .replace(/<tbody>/g, '')
      .replace(/<\/tbody>/g, '')
      .replace(/<tr>/g, '')
      .replace(/<th>/g, '')
      .replace(/<\/th>/g, ';')
      .replace(/<td>/g, '')
      .replace(/<\/td>/g, ';')
      .replace(/\s/g, '')
      .replace(/<\/tr>/g, '\r\n');
  var mylink = document.createElement('a');
  date= new Date().toLocaleDateString('pl', {day: 'numeric', month: 'numeric',year: 'numeric'});
  mylink.download = date+".csv";
  mylink.href = "data:application/csv," + escape(data);
  mylink.click();
}
